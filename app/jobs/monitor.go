package jobs

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/mail"
	"net/smtp"
	"strconv"
	"strings"
	"time"

	"github.com/revel/modules/jobs/app/jobs"
	"github.com/revel/modules/orm/gorp/app"
	"gitlab.com/nologicor/gdf-alarm/app/models"
)

// Monitor rigs
type Monitor struct {
	FCM *models.FCM
}

func Init(fcm *models.FCM) {
	jobs.Schedule("@every 1m", Monitor{
		FCM: fcm,
	})
}

func (c Monitor) Run() {

	rigs, err := gorp.Db.Map.Select(models.Rig{},
		`select * from Rig`)
	if err != nil {
		panic(err)
	}

	rigEmailData := []byte("")
	notifyAfterMins := 1
	for _, rigInterface := range rigs {
		rig := rigInterface.(*models.Rig)

		farmsInterface, err := gorp.Db.Map.Select(models.Farm{}, "select * from Farm where Id="+strconv.Itoa(rig.FarmId))
		farmName := ""
		for _, farmInterface := range farmsInterface {
			farm := farmInterface.(*models.Farm)
			farmName = farm.Name
		}
		resp, err := http.Get(rig.IP)
		if err != nil {
			fmt.Printf("Can't get response from rig!!!")
			gorp.Db.ExecUpdate(gorp.Db.Builder().Update("Rig").Set("Active", false).Set("InactiveFor", rig.InactiveFor+1).Where("Id=?", rig.Id))
			//add to rigEmailData array
			if rig.InactiveFor == notifyAfterMins-1 {
				data := "<tr><td>" + farmName + "</td><td>" + rig.Name + "</td></tr>"
				rigEmailData = append(rigEmailData, data...)
			}
		} else {
			bodyByte, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				fmt.Printf("Can't read resp.Body")
				gorp.Db.ExecUpdate(gorp.Db.Builder().Update("Rig").Set("Active", false).Where("Id=?", rig.Id))
			}
			bodyString := string(bodyByte)
			if strings.Contains(bodyString, "{\"result\":") {
				fmt.Println("====================================================================================================")
				strings1 := strings.Split(bodyString, "{")
				strings2 := strings.Split(string(strings1[1]), "}")
				result := string(strings2[0])
				fmt.Println(result)
				fmt.Println("====================================================================================================")
				gorp.Db.ExecUpdate(gorp.Db.Builder().Update("Rig").Set("Active", true).Set("InactiveFor", 0).Where("Id=?", rig.Id))
			} else {
				gorp.Db.ExecUpdate(gorp.Db.Builder().Update("Rig").Set("Active", false).Set("InactiveFor", rig.InactiveFor+1).Where("Id=?", rig.Id))
				//add to rigEmailData array
				if rig.InactiveFor == notifyAfterMins-1 {
					data := "<tr><td>" + farmName + "</td><td>" + rig.Name + "</td></tr>"
					rigEmailData = append(rigEmailData, data...)
				}
			}

			resp.Body.Close()
		}
	}

	//If rigEmailData array not empthy send email with rig names
	if len(rigEmailData) != 0 {
		SendEmail(string(rigEmailData))
		SendNotification(string(rigEmailData), c.FCM)
	}
}

func SendNotification(data string, fmc *models.FCM) {
	fmt.Println("+++++++++++++++++++++++++++++++++++++++>>>")
	fmt.Println(fmc)
	fmc.SendNotification(data)
}

func SendEmail(data string) {

	usersInterface, err := gorp.Db.Map.Select(models.User{},
		`select * from User`)
	if err != nil {
		panic(err)
	}

	for _, userInterface := range usersInterface {
		pass := "ZhBFn27JkFY85t4f"
		from := "gdf.alarm@gmail.com"
		to := userInterface.(*models.User).Email
		subject := "Inactive Rigs"
		date := time.Now().UTC()
		message := "From: " + from + "\n" +
			"To: " + to + "\n" +
			"Subject: " + subject + " " + date.Format("02.01.2006 15:04:05") + "\n" +
			"MIME-Version: 1.0\n" +
			"Content-Type: text/html; charset=\"utf-8\"\n\n" +
			`<!DOCTYPE html>
			<html>
				<head>
					<title>Notification</title>
					<style>
						table {
    						border-collapse: collapse;
						}
						table, th, td {
    						border: 1px solid black;
						}
					</style>
				</head>
				<body>
					<table>
						<tr>
							<th>Farm</th><th>Inactive Rig</th>
						</tr>` + data + `
					</table>
				</body>
			</html>`

		err = smtp.SendMail(
			"smtp.gmail.com:587",
			smtp.PlainAuth("", from, pass, "smtp.gmail.com"),
			from,
			[]string{to},
			[]byte(message))

		if err != nil {
			log.Printf("smtp error: %s", err)
			return
		}
		fmt.Println("Email sent to: " + to)
	}

}

func encodeRFC2047(String string) string {
	// use mail's rfc2047 to encode any string
	addr := mail.Address{String, ""}
	return strings.Trim(addr.String(), " <>")
}
