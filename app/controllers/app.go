package controllers

import (
	"golang.org/x/crypto/bcrypt"

	"github.com/revel/revel"

	"database/sql"
	"fmt"

	gorpController "github.com/revel/modules/orm/gorp/app/controllers"
	"gitlab.com/nologicor/gdf-alarm/app"
	"gitlab.com/nologicor/gdf-alarm/app/models"
	"gitlab.com/nologicor/gdf-alarm/app/routes"
)

type Application struct {
	gorpController.Controller
}

func (c Application) AddUser() revel.Result {
	if user := c.connected(); user != nil {
		c.ViewArgs["user"] = user
	}
	return nil
}

func (c Application) connected() *models.User {
	if c.ViewArgs["user"] != nil {
		return c.ViewArgs["user"].(*models.User)
	}
	if username, ok := c.Session["user"]; ok {
		return c.getUser(username.(string))
	}
	return nil
}

func (c Application) getUser(username string) (user *models.User) {
	user = &models.User{}
	fmt.Println("get user", username, c.Txn)

	err := c.Txn.SelectOne(user, c.Db.SqlStatementBuilder.Select("*").From("User").Where("Username=?", username))
	if err != nil {
		if err != sql.ErrNoRows {
			c.Log.Error("Failed to find user")
		}
		return nil
	}
	return
}

func (c Application) Index() revel.Result {
	if c.connected() != nil {
		return c.Redirect(routes.Farms.Index())
	}
	c.Flash.Error("Please log in first")
	return c.Render()
}

func (c Application) Register() revel.Result {
	return c.Render()
}

func (c Application) Login(username, password string, remember bool) revel.Result {
	user := c.getUser(username)
	if user != nil {
		err := bcrypt.CompareHashAndPassword(user.HashedPassword, []byte(password))
		if err == nil {
			c.Session["user"] = username
			if remember {
				c.Session.SetDefaultExpiration()
			} else {
				c.Session.SetNoExpiration()
			}
			c.Flash.Success("Welcome, " + username)
			return c.Redirect(routes.Farms.Index())
		}
	}

	c.Flash.Out["username"] = username
	c.Flash.Error("Login failed")
	return c.Redirect(routes.Application.Index())
}

func (c Application) Logout() revel.Result {
	for k := range c.Session {
		delete(c.Session, k)
	}

	return c.Redirect(routes.Application.Index())
}

func (c Application) SetToken(token string) revel.Result {
	app.Fcm.SetToken(token)
	fmt.Println("=============================================>>>")
	fmt.Println(token)
	return nil
}
