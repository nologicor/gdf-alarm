package controllers

import "github.com/revel/revel"

func init() {
	revel.InterceptMethod(Application.AddUser, revel.BEFORE)
	revel.InterceptMethod(Rigs.checkUser, revel.BEFORE)
	revel.InterceptMethod(Users.checkUser, revel.BEFORE)
}	
