package controllers

import (
	"strings"

	"github.com/revel/revel"
	"gitlab.com/nologicor/gdf-alarm/app/models"
	"gitlab.com/nologicor/gdf-alarm/app/routes"
	squirrel "gopkg.in/Masterminds/squirrel.v1"
)

type Farms struct {
	Application
}

func (c Farms) checkUser() revel.Result {
	if user := c.connected(); user == nil {
		c.Flash.Error("Please log in first")
		return c.Redirect(routes.Application.Index())
	}
	return nil
}

func (c Farms) Index() revel.Result {
	var farms []*models.Farm
	return c.Render(farms)
}

func (c Farms) List(search string, size, page uint64) revel.Result {
	if page == 0 {
		page = 1
	}
	nextPage := page + 1
	prevPage := page - 1
	search = strings.TrimSpace(search)

	var farms []*models.Farm
	builder := c.Db.SqlStatementBuilder.Select("*").From("Farm").Offset((page - 1) * size).Limit(size)
	if search != "" {
		search = "%" + strings.ToLower(search) + "%"
		builder = builder.Where(squirrel.Or{
			squirrel.Expr("lower(Name) like ?", search),
			squirrel.Expr("lower(IP) like ?", search)})
	}
	if _, err := c.Txn.Select(&farms, builder); err != nil {
		c.Log.Fatal("Unexpected error loading Farms", "error", err)
	}

	return c.Render(farms, search, size, page, nextPage, prevPage)
}

func (c Farms) loadFarmById(id int) *models.Farm {
	h, err := c.Txn.Get(models.Farm{}, id)
	if err != nil {
		panic(err)
	}
	if h == nil {
		return nil
	}
	return h.(*models.Farm)
}

func (c Farms) Show(id int) revel.Result {
	Farm := c.loadFarmById(id)
	if Farm == nil {
		return c.NotFound("Farm %d does not exist", id)
	}
	title := Farm.Name
	return c.Render(title, Farm)
}

func (c Farms) Delete(id int) revel.Result {
	farm := c.loadFarmById(id)
	if farm == nil {
		return c.NotFound("Farm %d does not exist", id)
	}
	_, err := c.Txn.Delete(&models.Farm{Id: id})
	if err != nil {
		panic(err)
	}

	c.DeleteAllInFarm(id)

	c.Flash.Success("Removed farm %s from monitoring.", strings.ToUpper(farm.Name))
	return c.Redirect(routes.Farms.Index())
}

func (c Farms) DeleteAllInFarm(farmId int) {
	var rigs []*models.Rig
	builder := c.Db.SqlStatementBuilder.Select("*").From("Rig").Where("FarmId =?", farmId)
	if _, err := c.Txn.Select(&rigs, builder); err != nil {
		c.Log.Fatal("Unexpected error loading Rigs", "error", err)
	}

	for i := 0; i < len(rigs); i++ {
		_, err := c.Txn.Delete(&models.Rig{Id: rigs[i].Id})
		if err != nil {
			panic(err)
		}
	}
}

func (c Farms) Create() revel.Result {
	farm := new(models.Farm)
	farm.Name = "test"
	return c.Render(farm)
}

func (c Farms) Save(farm models.Farm) revel.Result {
	farm.Validate(c.Validation)

	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(routes.Farms.Create())
	}

	err := c.Txn.Insert(&farm)
	if err != nil {
		panic(err)
	}
	c.Flash.Success("Farm %s added to monitoring.", strings.ToUpper(farm.Name))
	return c.Redirect(routes.Farms.Index())
}
