package controllers

import (
	"github.com/revel/revel"
	"golang.org/x/crypto/bcrypt"

	"gitlab.com/nologicor/gdf-alarm/app/models"
	"gitlab.com/nologicor/gdf-alarm/app/routes"
)

type Users struct {
	Application
}

func (c Users) checkUser() revel.Result {
	if user := c.connected(); user == nil {
		c.Flash.Error("Please log in first")
		return c.Redirect(routes.Application.Index())
	}
	return nil
}

func (c Users) Update() revel.Result {
	return c.Render()
}

func (c Users) SavePassword(password, verifyPassword string) revel.Result {
	models.ValidatePassword(c.Validation, password)
	c.Validation.Required(verifyPassword).
		Message("Please verify your password")
	c.Validation.Required(verifyPassword == password).
		Message("Your password doesn't match")

	if c.Validation.HasErrors() {
		c.Validation.Keep()
		return c.Redirect(routes.Users.Update())
	}

	bcryptPassword, _ := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	_, err := c.Txn.ExecUpdate(c.Db.SqlStatementBuilder.
		Update("User").Set("HashedPassword", bcryptPassword).
		Where("UserId=?", c.connected().UserId))
	if err != nil {
		panic(err)
	}
	c.Flash.Success("Password updated")
	return c.Redirect(routes.Users.Update())
}

func (c Users) SaveEmail(email string) revel.Result {
	c.Validation.Email(email)
	if c.Validation.HasErrors() {
		c.Validation.Keep()
		return c.Redirect(routes.Users.Update())
	}

	_, err := c.Txn.ExecUpdate(c.Db.SqlStatementBuilder.
		Update("User").Set("Email", email).
		Where("UserId=?", c.connected().UserId))
	if err != nil {
		panic(err)
	}
	c.Flash.Success("Email updated")
	return c.Redirect(routes.Users.Update())
}

func (c Users) CreateUser(user models.User, verifyPassword string) revel.Result {
	c.Validation.Required(verifyPassword)
	c.Validation.Required(verifyPassword == user.Password).
		MessageKey("Password does not match")
	user.Validate(c.Validation)

	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(routes.Users.Update())
	}

	user.HashedPassword, _ = bcrypt.GenerateFromPassword(
		[]byte(user.Password), bcrypt.DefaultCost)
	err := c.Txn.Insert(&user)
	if err != nil {
		panic(err)
	}

	c.Session["user"] = user.Username
	c.Flash.Success("Welcome, " + user.Username)
	return c.Redirect(routes.Farms.Index())
}
