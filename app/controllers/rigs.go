package controllers

import (
	"strings"

	"github.com/revel/revel"

	"gitlab.com/nologicor/gdf-alarm/app/models"
	"gitlab.com/nologicor/gdf-alarm/app/routes"

	"gopkg.in/Masterminds/squirrel.v1"
)

type Rigs struct {
	Application
}

func (c Rigs) checkUser() revel.Result {
	if user := c.connected(); user == nil {
		c.Flash.Error("Please log in first")
		return c.Redirect(routes.Application.Index())
	}
	return nil
}

func (c Rigs) Index(farmId int) revel.Result {
	var rigs []*models.Rig
	return c.Render(rigs, farmId)
}

func (c Rigs) List(search string, size, page uint64, farmId int) revel.Result {
	if page == 0 {
		page = 1
	}
	nextPage := page + 1
	prevPage := page - 1
	search = strings.TrimSpace(search)

	var rigs []*models.Rig
	builder := c.Db.SqlStatementBuilder.Select("*").From("Rig").Where("FarmId =?", farmId).Offset((page - 1) * size).Limit(size)
	if search != "" {
		search = "%" + strings.ToLower(search) + "%"
		builder = builder.Where(squirrel.Or{
			squirrel.Expr("lower(Name) like ?", search),
			squirrel.Expr("lower(IP) like ?", search)})
	}
	if _, err := c.Txn.Select(&rigs, builder); err != nil {
		c.Log.Fatal("Unexpected error loading Rigs", "error", err)
	}

	return c.Render(rigs, search, size, page, nextPage, prevPage, farmId)
}

func (c Rigs) loadRigById(id int) *models.Rig {
	h, err := c.Txn.Get(models.Rig{}, id)
	if err != nil {
		panic(err)
	}
	if h == nil {
		return nil
	}
	return h.(*models.Rig)
}

func (c Rigs) Show(id int) revel.Result {
	Rig := c.loadRigById(id)
	if Rig == nil {
		return c.NotFound("Rig %d does not exist", id)
	}
	title := Rig.Name
	return c.Render(title, Rig)
}

func (c Rigs) Delete(id int, farmId int) revel.Result {
	rig := c.loadRigById(id)
	if rig == nil {
		return c.NotFound("Rig %d does not exist", id)
	}
	_, err := c.Txn.Delete(&models.Rig{Id: id})
	if err != nil {
		panic(err)
	}
	c.Flash.Success("Removed rig %s from monitoring.", strings.ToUpper(rig.Name))
	return c.Redirect(routes.Rigs.Index(farmId))
}

// func (c Rigs) Settings() revel.Result {
// 	return c.Render()
// }

func (c Rigs) Create(farmId int) revel.Result {
	// rig := new(models.Rig)
	// rig.Track = true
	// rig.FarmId = farmId
	return c.Render(farmId)
}

func (c Rigs) Save(rig models.Rig) revel.Result {
	rig.Active = true
	rig.InactiveFor = 0

	rig.Validate(c.Validation)

	if c.Validation.HasErrors() {
		c.Validation.Keep()
		c.FlashParams()
		return c.Redirect(routes.Rigs.Create(rig.FarmId))
	}

	err := c.Txn.Insert(&rig)
	if err != nil {
		panic(err)
	}
	c.Flash.Success("Rig %s added to monitoring.", strings.ToUpper(rig.Name))
	return c.Redirect(routes.Rigs.Index(rig.FarmId))
}
