package models

import (
	gorp "github.com/revel/modules/orm/gorp/app"
	"github.com/revel/revel"
)

type Rig struct {
	Id          int
	Name        string
	IP          string
	Track       bool
	Active      bool
	InactiveFor int
	FarmId      int
}

func (rig *Rig) Validate(v *revel.Validation) {
	v.Check(rig.Name,
		revel.Required{},
		revel.MaxSize{100},
	)
	ValidateName(v, rig.Name)

	v.Check(rig.IP,
		revel.Required{},
		revel.MaxSize{100},
	)

	v.Check(rig.FarmId,
		revel.Required{},
	)
}

func ValidateName(v *revel.Validation, name string) {
	rigs, err := gorp.Db.Map.Select(Rig{}, "SELECT * FROM rig WHERE name = ?", name)
	if err != nil {
		panic(err)
	}
	if len(rigs) != 0 {
		v.Errors = append(v.Errors, &revel.ValidationError{
			Message: "Sorry, that name is already in use",
			Key:     "rig.Name",
		})
	}
}
