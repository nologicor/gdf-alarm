package models

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
)

type FCM struct {
	ServerKey  string
	FCMToken   string
	ServiceURL string
}

func (fcm *FCM) Init(serverKey string, serviceURL string) {
	fcm.ServerKey = serverKey
	fcm.ServiceURL = serviceURL
}

func (fcm *FCM) SetToken(token string) {
	fcm.FCMToken = token
}

func (fcm *FCM) SendNotification(data string) {
	url := fcm.ServiceURL
	serverKey := fcm.ServerKey
	token := fcm.FCMToken

	var jsonStr = []byte(`{"to":"` + token + `","notification":{"title": "Inactive Rigs","body":"` + data + `"}}`)
	req, err := http.NewRequest("POST", url, bytes.NewBuffer(jsonStr))
	req.Header.Set("Authorization", "key="+serverKey)
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	fmt.Println("response Status:", resp.Status)
	fmt.Println("response Headers:", resp.Header)
	body, _ := ioutil.ReadAll(resp.Body)
	fmt.Println("response Body:", string(body))
	return
}
