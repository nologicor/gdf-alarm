package models

import (
	"github.com/revel/revel"
)

type Farm struct {
	Id   int
	Name string
}

func (farm *Farm) Validate(v *revel.Validation) {
	v.Check(farm.Name,
		revel.Required{},
		revel.MaxSize{100},
	)
}
